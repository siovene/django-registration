��          �      <      �  )   �     �     �  -   �          (     9  _   S  %   �  N   �  U   (     ~  '   �     �     �     �     �  V  �  '   E     m     ~  -   �     �     �     �  j   �  '   a  T   �  V   �     5  ,   G     t     �     �     �                                                                	                   
                  A user with that username already exists. Activate users E-mail I have read and agree to the Terms of Service Password Password (again) Re-send activation emails Registration using free email addresses is prohibited. Please supply a different email address. The two password fields didn't match. This email address is already in use. Please supply a different email address. This value may contain only letters, numbers and underscores. Spaces are not allowed. Username You must agree to the terms to register activation key registration profile registration profiles user Project-Id-Version: django-registration 0.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-01-03 07:41-0500
PO-Revision-Date: 2012-01-03 12:30+0100
Last-Translator: José J. Chambó <jjchambo@hotmail.com>
Language-Team: Español <de@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Ya existe otro usuario con este nombre. Activar usuarios E-mail He leído y acepto los Términos del Servicio Contraseña Contraseña (otra vez) Reenviar correos de activación No se permite el registro usando una dirección de email no válida.Por favor proporciona otra dirección. Los campos de contraseña no coinciden. Esta dirección de email ya está siendo usada. Por favor introduce otra dirección. Sólo puede contener letras, números y guiones bajos. Los espacios no son permitidos. Nombre de usuario Debes aceptar los términos para registrarte clave de activación perfil de registro perfiles de registro usuario 