��          �      ,      �  )   �     �  -   �     �     �     
  _   $  %   �  N   �  U   �     O  '   X     �     �     �     �  \  �  )        F  <   ^     �     �  ,   �  d   �  ,   W  L   �  w   �     I  C   [     �     �     �     �               
                             	                                  A user with that username already exists. Activate users I have read and agree to the Terms of Service Password Password (again) Re-send activation emails Registration using free email addresses is prohibited. Please supply a different email address. The two password fields didn't match. This email address is already in use. Please supply a different email address. This value may contain only letters, numbers and underscores. Spaces are not allowed. Username You must agree to the terms to register activation key registration profile registration profiles user Project-Id-Version: django-registration 0.8 alpha-1 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-01-08 12:57-0500
PO-Revision-Date: 2010-07-01 14:30+0200
Last-Translator: Patrick Samson <maxcom@laposte.net>
Language-Team: Français <fr@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Un utilisateur avec ce nom existe déjà. Active les utilisateurs J'ai lu et accepté les Conditions Générales d'Utilisation Mot de passe Mot de passe (vérification) Envoie à nouveau les courriels d'activation L'inscription avec adresse courriel de compte gratuit est interdite. Veuillez en indiquer une autre. Les deux mots de passe ne correspondent pas. Cette adresse courriel est déjà utilisée. Veuillez en indiquer une autre. Cette valeur ne doit contenir que des lettres, chiffres et caractères de soulignement. Espaces ne sont pas autorisés. Nom d'utilisateur Vous devez accepter les conditions d'utilisation pour vous inscrire clé d'activation profil d'inscription profils d'inscription utilisateur 