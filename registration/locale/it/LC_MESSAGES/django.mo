��          �      ,      �  )   �     �  -   �     �     �     
  _   $  %   �  N   �  U   �     O  '   X     �     �     �     �  �  �  !   |     �  ?   �     �     �     	  `   '  $   �  J   �  `   �     Y  /   e     �     �     �     �               
                             	                                  A user with that username already exists. Activate users I have read and agree to the Terms of Service Password Password (again) Re-send activation emails Registration using free email addresses is prohibited. Please supply a different email address. The two password fields didn't match. This email address is already in use. Please supply a different email address. This value may contain only letters, numbers and underscores. Spaces are not allowed. Username You must agree to the terms to register activation key registration profile registration profiles user Project-Id-Version: django-registration 0.8 alpha-1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-12-17 11:06-0500
PO-Revision-Date: 2011-12-17 11:07-0500
Last-Translator: Salvatore Iovene <salvatore@iovene.com>
Language-Team: Italiano <it@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Language: Italian
X-Poedit-Country: ITALY
 Questo nome utente è già usato. Attiva utenti Dichiaro di aver letto e di approvare le Condizioni di Servizio Password Password (di nuovo) Re-invia email di attivazione La registrazione con indirizzi email gratis non è permessa. Inserisci un altro indirizzo email. Le password inserite non coincidono. Questo indirizzo email è già in uso. Inserisci un altro indirizzo email. Questo valore può contenere solo lettere, numeri e sottolineature. Gli spazi non sono permessi. Nome utente Per registrarsi bisogna approvare le condizioni chiave di attivazione profilo di registrazione profili di registrazione utente 